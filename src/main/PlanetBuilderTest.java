package main;

import java.util.ArrayList;
import java.util.List;

import main.planets.Planet;

public class PlanetBuilderTest {

	public static void main(String[] args) {
		List<Planet> planets = new ArrayList<Planet>();
		
		Planet earth = new Planet.Builder("Earth", 6000, 10)
				.build();
		Planet mars = new Planet.Builder("Mars", 3500, 6)
				.numberOfMoons(2)
				.build();
		Planet saturn = new Planet.Builder("Saturn", 60000, 100)
				.numberOfMoons(62)
				.numberOfRings(10)
				.build();
		
		planets.add(earth);
		planets.add(mars);
		planets.add(saturn);
		
		for (Planet planet : planets) {
			System.out.println(planet);
		}
	}
	
}
