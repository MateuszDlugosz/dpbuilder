package main.planets;

public class Planet {

	private String name;
	
	private float radius;
	private float mass;
	
	private int numberOfMoons;
	private int numberOfRings;
	
	private float diameter;
	
	Planet(Builder builder) {
		name = builder.name;
		radius = builder.radius;
		mass = builder.mass;
		numberOfMoons = builder.numberOfMoons;
		numberOfRings = builder.numberOfRings;
		diameter = builder.radius * 2;
	}
	
	public String getName() {
		return name;
	}
	
	public float getRadius() {
		return radius;
	}

	public float getMass() {
		return mass;
	}

	public int getNumberOfMoons() {
		return numberOfMoons;
	}

	public int getNumberOfRings() {
		return numberOfRings;
	}

	public float getDiameter() {
		return diameter;
	}

	@Override
	public String toString() {
		String description = "";
		
		description += "name: " + name + " ";
		description += "radius: " + radius + " ";
		description += "diameter: " + diameter + " ";
		description += "numberOfMoons: " + numberOfMoons + " ";
		description += "numberOfRings: " + numberOfRings + " ";
		
		return description;
	}
	
	public static class Builder {
		
		private String name;
		private final float radius;
		private final float mass;
		
		private int numberOfMoons = 0;
		private int numberOfRings = 0;
		
		public Builder(final String name, final float radius, final float mass) {
			this.name = name;
			this.radius = radius;
			this.mass = mass;
		}
		
		public Builder numberOfMoons(int numberOfMoons) {
			this.numberOfMoons = numberOfMoons;
			return this;
		}
		
		public Builder numberOfRings(int numberOfRings) {
			this.numberOfRings = numberOfRings;
			return this;
		}
		
		public Planet build() {
			return new Planet(this);
		}
		
	}
	
}
